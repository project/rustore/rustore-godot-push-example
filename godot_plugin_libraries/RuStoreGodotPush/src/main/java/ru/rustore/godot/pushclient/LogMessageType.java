package ru.rustore.godot.pushclient;

public enum LogMessageType {
    DEBUG,
    ERROR,
    INFO,
    VERBOSE,
    WARN
}

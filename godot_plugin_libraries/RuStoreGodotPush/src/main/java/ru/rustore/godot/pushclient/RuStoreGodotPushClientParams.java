package ru.rustore.godot.pushclient;

import androidx.annotation.NonNull;
import android.content.Context;
import java.util.Map;
import ru.rustore.sdk.pushclient.provider.AbstractRuStorePushClientParams;

public class RuStoreGodotPushClientParams extends AbstractRuStorePushClientParams {
    public final String METRIC_TYPE = "godot";

    public RuStoreGodotPushClientParams(@NonNull Context context) {
        super(context);
    }

    @Override
    final public Map<String, Object> getInternalConfig() {
        return Map.of("type", METRIC_TYPE);
    }
}

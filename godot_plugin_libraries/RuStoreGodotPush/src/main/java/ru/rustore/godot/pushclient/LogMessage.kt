package ru.rustore.godot.pushclient

data class LogMessage(
    val type: LogMessageType,
    val message: String,
    val throwable: Throwable?
)

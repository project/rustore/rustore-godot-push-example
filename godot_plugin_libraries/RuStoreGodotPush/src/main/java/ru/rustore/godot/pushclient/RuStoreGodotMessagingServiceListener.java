package ru.rustore.godot.pushclient;

import java.util.List;

import ru.rustore.sdk.pushclient.messaging.exception.RuStorePushClientException;
import ru.rustore.sdk.pushclient.messaging.model.RemoteMessage;

public interface RuStoreGodotMessagingServiceListener {

    void OnNewToken(String token);
    void OnMessageReceived(RemoteMessage message);
    void OnDeletedMessages();
    void OnError(List<RuStorePushClientException> errors);
}

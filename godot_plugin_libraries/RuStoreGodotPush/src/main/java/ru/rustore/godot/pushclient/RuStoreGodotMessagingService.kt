package ru.rustore.godot.pushclient

import ru.rustore.sdk.pushclient.messaging.exception.RuStorePushClientException
import ru.rustore.sdk.pushclient.messaging.model.RemoteMessage
import ru.rustore.sdk.pushclient.messaging.service.RuStoreMessagingService
import java.util.concurrent.locks.ReentrantLock

class RuStoreGodotMessagingService : RuStoreMessagingService() {
    companion object {
        private var messages: MutableList<ServiceMessage> = ArrayList()

        private var externListener: RuStoreGodotMessagingServiceListener? = null
        private val lock = ReentrantLock()

        fun setListener(value: RuStoreGodotMessagingServiceListener?) {
            lock.lock()
            try {
                externListener = value
                ship()
            } finally {
                lock.unlock()
            }
        }

        private fun ship() {
            externListener?.let { listener ->
                messages.forEach { message ->
                    when (message) {
                        is ServiceMessage.Token -> listener.OnNewToken(message.token)
                        is ServiceMessage.Message -> listener.OnMessageReceived(message.message)
                        is ServiceMessage.Deleted -> listener.OnDeletedMessages()
                        is ServiceMessage.Error -> listener.OnError(message.errors)
                    }
                }
                messages.clear()
            }
        }
    }

    override fun onNewToken(token: String) {
        lock.lock()
        try {
            externListener?.OnNewToken(token)
            collect(ServiceMessage.Token(token))
        } finally {
            lock.unlock()
        }
    }

    override fun onMessageReceived(message: RemoteMessage) {
        lock.lock()
        try {
            externListener?.OnMessageReceived(message)
            collect(ServiceMessage.Message(message))
        } finally {
            lock.unlock()
        }
    }

    override fun onDeletedMessages() {
        lock.lock()
        try {
            externListener?.OnDeletedMessages()
            collect(ServiceMessage.Deleted)
        } finally {
            lock.unlock()
        }
    }

    override fun onError(errors: List<RuStorePushClientException>) {
        lock.lock()
        try {
            externListener?.OnError(errors)
            collect(ServiceMessage.Error(errors))
        } finally {
            lock.unlock()
        }
    }

    private fun collect(serviceMessage: ServiceMessage) {
        if (externListener == null) {
            messages.add(serviceMessage)
        }
    }
}

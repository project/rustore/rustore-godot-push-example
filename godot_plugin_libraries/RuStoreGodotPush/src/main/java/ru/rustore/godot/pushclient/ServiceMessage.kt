package ru.rustore.godot.pushclient

import ru.rustore.sdk.pushclient.messaging.exception.RuStorePushClientException
import ru.rustore.sdk.pushclient.messaging.model.RemoteMessage

sealed class ServiceMessage {
    data class Token(val token: String) : ServiceMessage()
    data class Message(val message: RemoteMessage) : ServiceMessage()
    object Deleted : ServiceMessage()
    data class Error(val errors: List<RuStorePushClientException>) : ServiceMessage()
}

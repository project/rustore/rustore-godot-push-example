package ru.rustore.godot.pushclient

import android.net.Uri
import android.util.Log
import androidx.collection.ArraySet
import com.google.gson.GsonBuilder
import org.godotengine.godot.Dictionary
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo
import org.godotengine.godot.plugin.UsedByGodot
import ru.rustore.godot.core.JsonBuilder
import ru.rustore.godot.core.UriTypeAdapter
import ru.rustore.sdk.core.exception.RuStoreException
import ru.rustore.sdk.core.feature.model.FeatureAvailabilityResult
import ru.rustore.sdk.pushclient.RuStorePushClient
import ru.rustore.sdk.pushclient.common.logger.Logger
import ru.rustore.sdk.pushclient.messaging.exception.RuStorePushClientException
import ru.rustore.sdk.pushclient.messaging.model.RemoteMessage
import ru.rustore.sdk.pushclient.utils.resolveForPush

class RuStoreGodotPush(godot: Godot?): GodotPlugin(godot), RuStoreGodotMessagingServiceListener, Logger {
	private companion object {
		const val PLUGIN_NAME = "RuStoreGodotPush"

		const val CHANNEL_CHECK_PUSH_AVAILABLE_SUCCESS = "rustore_check_push_available_success"
		const val CHANNEL_CHECK_PUSH_AVAILABLE_FAILURE = "rustore_check_push_available_failure"
		const val CHANNEL_GET_TOKEN_SUCCESS = "rustore_get_token_success"
		const val CHANNEL_GET_TOKEN_FAILURE = "rustore_get_token_failure"
		const val CHANNEL_DELETE_TOKEN_SUCCESS = "rustore_delete_token_success"
		const val CHANNEL_DELETE_TOKEN_FAILURE = "rustore_delete_token_failure"
		const val CHANNEL_SUBSCRIBE_TO_TOPIC_SUCCESS = "rustore_subscribe_to_topic_success"
		const val CHANNEL_SUBSCRIBE_TO_TOPIC_FAILURE = "rustore_subscribe_to_topic_failure"
		const val CHANNEL_UNSUBSCRIBE_FROM_TOPIC_SUCCESS = "rustore_unsubscribe_from_topic_success"
		const val CHANNEL_UNSUBSCRIBE_FROM_TOPIC_FAILURE = "rustore_unsubscribe_from_topic_failure"

		const val CHANNEL_ON_NEW_TOKEN = "rustore_on_new_token"
		const val CHANNEL_ON_MESSAGE_RECEIVED = "rustore_on_message_received"
		const val CHANNEL_ON_DELETED_MESSAGES = "rustore_on_deleted_messages"
		const val CHANNEL_ON_ERROR = "rustore_on_error"

		const val CHANNEL_ON_LOG_VERBOSE = "rustore_on_log_verbose"
		const val CHANNEL_ON_LOG_DEBUG = "rustore_on_log_debug"
		const val CHANNEL_ON_LOG_INFO = "rustore_on_log_info"
		const val CHANNEL_ON_LOG_WARN = "rustore_on_log_warn"
		const val CHANNEL_ON_LOG_ERROR = "rustore_on_log_error"
	}

	override fun getPluginName(): String {
		return PLUGIN_NAME
	}

	override fun getPluginSignals(): Set<SignalInfo> {
		val signals: MutableSet<SignalInfo> = ArraySet()
		signals.add(SignalInfo(CHANNEL_CHECK_PUSH_AVAILABLE_SUCCESS, String::class.java))
		signals.add(SignalInfo(CHANNEL_CHECK_PUSH_AVAILABLE_FAILURE, String::class.java))
		signals.add(SignalInfo(CHANNEL_GET_TOKEN_SUCCESS, String::class.java))
		signals.add(SignalInfo(CHANNEL_GET_TOKEN_FAILURE, String::class.java))
		signals.add(SignalInfo(CHANNEL_DELETE_TOKEN_SUCCESS))
		signals.add(SignalInfo(CHANNEL_DELETE_TOKEN_FAILURE, String::class.java))
		signals.add(SignalInfo(CHANNEL_SUBSCRIBE_TO_TOPIC_SUCCESS))
		signals.add(SignalInfo(CHANNEL_SUBSCRIBE_TO_TOPIC_FAILURE, String::class.java))
		signals.add(SignalInfo(CHANNEL_UNSUBSCRIBE_FROM_TOPIC_SUCCESS))
		signals.add(SignalInfo(CHANNEL_UNSUBSCRIBE_FROM_TOPIC_FAILURE, String::class.java))

		signals.add(SignalInfo(CHANNEL_ON_NEW_TOKEN, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_MESSAGE_RECEIVED, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_DELETED_MESSAGES))
		signals.add(SignalInfo(CHANNEL_ON_ERROR, Dictionary::class.java))

		signals.add(SignalInfo(CHANNEL_ON_LOG_VERBOSE, String::class.java, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_LOG_DEBUG, String::class.java, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_LOG_INFO, String::class.java, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_LOG_WARN, String::class.java, String::class.java))
		signals.add(SignalInfo(CHANNEL_ON_LOG_ERROR, String::class.java, String::class.java))

		return signals
	}

	private var allowErrorHandling: Boolean = false
	private val gson = GsonBuilder()
		.registerTypeAdapter(Uri::class.java, UriTypeAdapter())
		.create()

	@UsedByGodot
	fun setErrorHandling(allowErrorHandling: Boolean) {
		this.allowErrorHandling = allowErrorHandling
	}

	@UsedByGodot
	fun getErrorHandling(): Boolean {
		return allowErrorHandling
	}

	private fun handleError(throwable: Throwable?) {
		if (allowErrorHandling && throwable is RuStoreException) {
			godot.getActivity()?.let{ activity ->
				throwable.resolveForPush(activity)
			}
		}
	}

	@UsedByGodot
	fun init(allowNativeErrorHandling: Boolean) {
		allowErrorHandling = allowNativeErrorHandling
		RuStoreGodotMessagingService.setListener(this)
	}

	@UsedByGodot
	fun initLogger() {
		RuStoreGodotLogger.setExternLogger(this)
	}

	@UsedByGodot
	fun checkPushAvailability() {
		RuStorePushClient.checkPushAvailability()
			.addOnSuccessListener { result ->
				when (result) {
					is FeatureAvailabilityResult.Available -> {
						emitSignal(CHANNEL_CHECK_PUSH_AVAILABLE_SUCCESS, """{"isAvailable": true}""")
					}
					is FeatureAvailabilityResult.Unavailable -> {
						val cause = JsonBuilder.toJson(result.cause)
						val json = """{"isAvailable": false, "cause": $cause}"""
						emitSignal(CHANNEL_CHECK_PUSH_AVAILABLE_SUCCESS, json)
					}
				}
			}
			.addOnFailureListener { throwable ->
				handleError(throwable)
				emitSignal(CHANNEL_CHECK_PUSH_AVAILABLE_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	@UsedByGodot
	fun getToken() {
		RuStorePushClient.getToken()
			.addOnSuccessListener { result ->
				emitSignal(CHANNEL_GET_TOKEN_SUCCESS, result)
			}
			.addOnFailureListener{ throwable ->
				handleError(throwable)
				emitSignal(CHANNEL_GET_TOKEN_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	@UsedByGodot
	fun deleteToken() {
		RuStorePushClient.deleteToken()
			.addOnSuccessListener {
				emitSignal(CHANNEL_DELETE_TOKEN_SUCCESS)
			}
			.addOnFailureListener { throwable ->
				handleError(throwable)
				emitSignal(CHANNEL_DELETE_TOKEN_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	@UsedByGodot
	fun subscribeToTopic(topicName: String) {
		RuStorePushClient.subscribeToTopic(topicName)
			.addOnSuccessListener {
				emitSignal(CHANNEL_SUBSCRIBE_TO_TOPIC_SUCCESS)
			}
			.addOnFailureListener { throwable ->
				handleError(throwable)
				emitSignal(CHANNEL_SUBSCRIBE_TO_TOPIC_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	@UsedByGodot
	fun unsubscribeFromTopic(topicName: String) {
		RuStorePushClient.unsubscribeFromTopic(topicName)
			.addOnSuccessListener {
				emitSignal(CHANNEL_UNSUBSCRIBE_FROM_TOPIC_SUCCESS)
			}
			.addOnFailureListener { throwable ->
				handleError(throwable)
				emitSignal(CHANNEL_UNSUBSCRIBE_FROM_TOPIC_FAILURE, JsonBuilder.toJson(throwable))
			}
	}

	override fun OnNewToken(token: String) {
		emitSignal(CHANNEL_ON_NEW_TOKEN, token)
	}

	override fun OnMessageReceived(message: RemoteMessage?) {
		emitSignal(CHANNEL_ON_MESSAGE_RECEIVED, gson.toJson(message))
	}

	override fun OnDeletedMessages() {
		emitSignal(CHANNEL_ON_DELETED_MESSAGES)
	}

	override fun OnError(errors: MutableList<RuStorePushClientException>?) {
		val response = Dictionary()
		if (errors != null) {
			for ((index, e) in errors.withIndex()) {
				runOnUiThread {
					handleError(e)
				}
				response[index.toString()] = JsonBuilder.toJson(e)
			}
		}
		emitSignal(CHANNEL_ON_ERROR, response)
	}

	override fun debug(message: String, throwable: Throwable?) {
		emitSignal(CHANNEL_ON_LOG_DEBUG, message, JsonBuilder.toJson(throwable))
	}

	override fun error(message: String, throwable: Throwable?) {
		emitSignal(CHANNEL_ON_LOG_ERROR, message, JsonBuilder.toJson(throwable))
	}

	override fun info(message: String, throwable: Throwable?) {
		emitSignal(CHANNEL_ON_LOG_INFO, message, JsonBuilder.toJson(throwable))
	}

	override fun verbose(message: String, throwable: Throwable?) {
		emitSignal(CHANNEL_ON_LOG_VERBOSE, message, JsonBuilder.toJson(throwable))
	}

	override fun warn(message: String, throwable: Throwable?) {
		emitSignal(CHANNEL_ON_LOG_WARN, message, JsonBuilder.toJson(throwable))
	}

	override fun createLogger(tag: String): Logger = this
}

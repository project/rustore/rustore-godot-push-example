extends Node2D

@onready var _topic = $CanvasLayer/VBoxContainer/BottomButtons/TopicCommands/Topic
@onready var _token = $CanvasLayer/VBoxContainer/TopLine/Token
@onready var _copy = $CanvasLayer/VBoxContainer/TopLine/Copy
@onready var _log = $CanvasLayer/VBoxContainer/Log
@onready var _loading = $CanvasLayer/LoadingPanel

var _core_client: RuStoreGodotCoreUtils = null
var _push_client: RuStoreGodotPushClient = null

# Called when the node enters the scene tree for the first time.
func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	_push_client = RuStoreGodotPushClient.get_instance()
	_push_client.on_check_push_availability_success.connect(_on_check_push_availability_success)
	_push_client.on_check_push_availability_failure.connect(_on_check_push_availability_failure)
	_push_client.on_get_token_success.connect(_on_get_token_success)
	_push_client.on_get_token_failure.connect(_on_get_token_failure)
	_push_client.on_delete_token_success.connect(_on_delete_token_success)
	_push_client.on_delete_token_failure.connect(_on_delete_token_failure)
	_push_client.on_subscribe_to_topic_success.connect(_on_subscribe_to_topic_success)
	_push_client.on_subscribe_to_topic_failure.connect(_on_subscribe_to_topic_failure)
	_push_client.on_unsubscribe_from_topic_success.connect(_on_unsubscribe_from_topic_success)
	_push_client.on_unsubscribe_from_topic_failure.connect(_on_unsubscribe_from_topic_failure)
	
	_push_client.on_new_token.connect(_on_new_token)
	_push_client.on_message_received.connect(_on_message_received)
	_push_client.on_deleted_messages.connect(_on_deleted_messages)
	_push_client.on_error.connect(_on_error)
	
	# Logger events
	_push_client.on_log_verbose.connect(_on_log_verbose)
	_push_client.on_log_debug.connect(_on_log_debug)
	_push_client.on_log_info.connect(_on_log_info)
	_push_client.on_log_warn.connect(_on_log_warn)
	_push_client.on_log_error.connect(_on_log_error)
	
	_push_client.init(true)
	_push_client.init_logger()


func _on_copy_pressed():
	_core_client.copy_to_clipboard(_token.text)
	_core_client.show_toast("Copied")


# Check push availability
func _check_push_availability():
	_loading.visible = true
	_push_client.check_push_availability()

func _on_check_push_availability_success(result: RuStoreFeatureAvailabilityResult):
	_loading.visible = false
	_log.add_line(str(result.isAvailable) + " " + result.cause.description)

func _on_check_push_availability_failure(error: RuStoreError):
	_loading.visible = false
	_print_cause(error)


# Get token
func _get_token():
	_loading.visible = true
	_push_client.get_token()

func _on_get_token_success(data: String):
	_loading.visible = false
	_log.add_line("Token: " + data)
	_token.text = data

func _on_get_token_failure(error: RuStoreError):
	_loading.visible = false
	_print_cause(error)


# Delete token
func _delete_token():
	_loading.visible = true
	_push_client.delete_token()
	
func _on_delete_token_success():
	_loading.visible = false
	_log.add_line("Token deleted")
	_token.text = ""

func _on_delete_token_failure(error: RuStoreError):
	_loading.visible = false
	_print_cause(error)


# Sevice events
func _on_new_token(data: String):
	_log.add_line(data)

func _on_message_received(message: RuStoreRemoteMessage):	
	_log.add_line("======")
	_log.add_line("messageId: " + str(message.messageId))
	_log.add_line("priority: " + str(message.priority))
	_log.add_line("ttl: " + str(message.ttl))
	_log.add_line("from: " + message.from)
	_log.add_line("collapseKey: " + str(message.collapseKey))
	_log.add_line("data: " + JSON.stringify(message.data))
	_log.add_line("rawData: " + JSON.stringify(message.rawData))
	_log.add_line("notification:")
	_log.add_line("\ttitle: " + str(message.notificationData.title))
	_log.add_line("\tbody: " + str(message.notificationData.body))
	_log.add_line("\tchannelId: " + str(message.notificationData.channelId))
	_log.add_line("\tcolor: " + str(message.notificationData.color))
	_log.add_line("\ticon: " + str(message.notificationData.icon))
	_log.add_line("\tclickAction: " + str(message.notificationData.clickAction))
	_log.add_line("\timageUrl: " + str(message.notificationData.imageUrl))
	_log.add_line("======")

func _on_deleted_messages():
	_log.add_line("Deleted messages")

func _on_error(errors: Array):
	_log.add_line("=== Errors ===")
	for e in errors:
		_print_cause(e)
	_log.add_line("=== End ===")


# Log events
func _print_cause(cause: RuStoreError):
	if (cause != null):
		_log.add_line("Error: " + cause.name)
		_log.add_line("Description: " + cause.description)

func _on_log_verbose(message: String, cause: RuStoreError):
	_log.add_line("Log verbose: " + message)
	_print_cause(cause)

func _on_log_debug(message: String, cause: RuStoreError):
	_log.add_line("Log debug: " + message)
	_print_cause(cause)
	
func _on_log_info(message: String, cause: RuStoreError):
	_log.add_line("Log info: " + message)
	_print_cause(cause)
	
func _on_log_warn(message: String, cause: RuStoreError):
	_log.add_line("Log warn: " + message)
	_print_cause(cause)
	
func _on_log_error(message: String, cause: RuStoreError):
	_log.add_line("Log error: " + message)
	_print_cause(cause)
	

# Subscribe to topic
func _on_subscribe_to_topic_pressed():
	_loading.visible = true
	_push_client.subscribe_to_topic(_topic.text)

func _on_subscribe_to_topic_success():
	_loading.visible = false
	_log.add_line("Subscribe success")

func _on_subscribe_to_topic_failure(error: RuStoreError):
	_loading.visible = false
	_print_cause(error)


# Unsubscribe from topic
func _on_unsubscribe_from_topic_pressed():
	_loading.visible = true
	_push_client.unsubscribe_from_topic(_topic.text)
	
func _on_unsubscribe_from_topic_success():
	_loading.visible = false
	_log.add_line("Unsubscribe success")

func _on_unsubscribe_from_topic_failure(error: RuStoreError):
	_loading.visible = false
	_print_cause(error)

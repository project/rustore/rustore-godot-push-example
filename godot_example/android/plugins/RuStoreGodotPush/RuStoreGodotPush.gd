# RuStoreGodotPushClient
# @brief Класс реализует API для приёма push-сообщений через сервисы RuStore.
class_name RuStoreGodotPushClient extends Object

const SINGLETON_NAME = "RuStoreGodotPush"

var _isInitialized: bool = false
var _clientWrapper: Object = null

static var _instance: RuStoreGodotPushClient = null

# @brief Действие, выполняемое при успешном завершении операции check_push_availability.
signal on_check_push_availability_success

# @brief Действие, выполняемое в случае ошибки check_push_availability.
signal on_check_push_availability_failure

# @brief Действие, выполняемое при успешном завершении операции get_token.
signal on_get_token_success

# @brief Действие, выполняемое в случае ошибки get_token.
signal on_get_token_failure

# @brief Действие, выполняемое при успешном завершении операции delete_token.
signal on_delete_token_success

# @brief Действие, выполняемое в случае ошибки delete_token.
signal on_delete_token_failure

# @brief Действие, выполняемое при успешном завершении операции subscribe_to_topic.
signal on_subscribe_to_topic_success

# @brief Действие, выполняемое в случае ошибки subscribe_to_topic.
signal on_subscribe_to_topic_failure

# @brief Действие, выполняемое при успешном завершении операции unsubscribe_from_topic.
signal on_unsubscribe_from_topic_success

# @brief Действие, выполняемое в случае ошибки unsubscribe_from_topic.
signal on_unsubscribe_from_topic_failure


# @brief
#	Метод вызывается при получении нового push-токена.
#	После вызова этого метода приложение становится ответственно за передачу нового push-токена на свой сервер.
signal on_new_token

# @brief
#	Метод вызывается при получении нового push-уведомления.
#	Если в объекте notification есть данные, SDK самостоятельно отображает уведомление.
#	Если вы не хотите этого, используйте объект data, а notification оставьте пустым.
#	Метод вызывается в любом случае.
signal on_message_received

# @brief
#	Метод вызывается, если один или несколько push-уведомлений не доставлены на устройство.
#	Например, если время жизни уведомления истекло до момента доставки.
#	При вызове этого метода рекомендуется синхронизироваться со своим сервером, чтобы не пропустить данные.
signal on_deleted_messages

# @brief Метод вызывается, если в момент инициализации возникает ошибка.
signal on_error


# @brief
#	Логирует подробное сообщение.
#	Используется для записи детализированных логов, которые полезны для отладки.
signal on_log_verbose

# @brief
#	Логирует отладочное сообщение.
#	Используется для записи логов, которые помогают в процессе отладки приложения.
signal on_log_debug

# @brief
#	Логирует информационное сообщение.
#	Используется для записи стандартных логов, которые помогают отслеживать нормальное выполнение программы.
signal on_log_info

#  @brief
#	Логирует предупреждающее сообщение.
#	Используется для записи логов, которые сигнализируют о потенциальных проблемах,
#	которые не мешают выполнению программы, но могут потребовать внимания.
signal on_log_warn

# @brief
#	Логирует сообщение об ошибке.
#	Используется для записи логов, которые сигнализируют о возникновении ошибок,
#	требующих вмешательства или исправления.
signal on_log_error


# @brief
#	Получить экземпляр RuStoreGodotPushClient.
# @return
#	Возвращает указатель на единственный экземпляр RuStoreGodotPushClient (реализация паттерна Singleton).
#	Если экземпляр еще не создан, создает его.
static func get_instance() -> RuStoreGodotPushClient:
	if _instance == null:
		_instance = RuStoreGodotPushClient.new()
	return _instance


# @brief Выполняет инициализацию синглтона RuStoreGodotPushClient.
# @param allowNativeErrorHandling
#	Обработка ошибок в нативном SDK.
#	true — разрешает обработку ошибок, false — запрещает.
func init(allowNativeErrorHandling: bool = false):
	if _isInitialized == false && Engine.has_singleton(SINGLETON_NAME):
		_clientWrapper = Engine.get_singleton(SINGLETON_NAME)
		_connect_plugin_signals()
		_clientWrapper.init(allowNativeErrorHandling)
		_isInitialized = true


# @brief
#	Инициализация логгера.
#	Позволяет логировать события с использованием скриптинга Godot.
#	После инициализации плагина единожды выполните подписку на события логгера и воспользуйтесь методом init_logger.
func init_logger():
	_connect_logger_signals()
	_clientWrapper.initLogger()

# @brief Обработка ошибок в нативном SDK.
# @param allowNativeErrorHandling true — разрешает обработку ошибок, false — запрещает.
func set_error_handling(allowNativeErrorHandling: bool):
	_clientWrapper.setErrorHandling(allowNativeErrorHandling)

# @brief Получает текущее состояние обработки ошибок в нативном SDK.
# @return true — обработку ошибок разрешена, false — запрещена.
func get_error_handling() -> bool:
	return _clientWrapper.getErrorHandling()

func _connect_plugin_signals():
	_clientWrapper.rustore_check_push_available_success.connect(_on_check_push_availability_success)
	_clientWrapper.rustore_check_push_available_failure.connect(_on_check_push_availability_failure)
	_clientWrapper.rustore_get_token_success.connect(_on_get_token_success)
	_clientWrapper.rustore_get_token_failure.connect(_on_get_token_failure)
	_clientWrapper.rustore_delete_token_success.connect(_on_delete_token_success)
	_clientWrapper.rustore_delete_token_failure.connect(_on_delete_token_failure)
	_clientWrapper.rustore_subscribe_to_topic_success.connect(_on_subscribe_to_topic_success)
	_clientWrapper.rustore_subscribe_to_topic_failure.connect(_on_subscribe_to_topic_failure)
	_clientWrapper.rustore_unsubscribe_from_topic_success.connect(_on_unsubscribe_from_topic_success)
	_clientWrapper.rustore_unsubscribe_from_topic_failure.connect(_on_unsubscribe_from_topic_failure)
	
	_clientWrapper.rustore_on_new_token.connect(_on_new_token)
	_clientWrapper.rustore_on_message_received.connect(_on_message_received)
	_clientWrapper.rustore_on_deleted_messages.connect(_on_deleted_messages)
	_clientWrapper.rustore_on_error.connect(_on_error)

func _connect_logger_signals():
	_clientWrapper.rustore_on_log_verbose.connect(_on_log_verbose)
	_clientWrapper.rustore_on_log_debug.connect(_on_log_debug)
	_clientWrapper.rustore_on_log_info.connect(_on_log_info)
	_clientWrapper.rustore_on_log_warn.connect(_on_log_warn)
	_clientWrapper.rustore_on_log_error.connect(_on_log_error)


# Check push availability
# @brief
#	Проверка доступности приёма push-сообщений.
func check_push_availability():
	_clientWrapper.checkPushAvailability()

func _on_check_push_availability_success(data: String):
	var obj = RuStoreFeatureAvailabilityResult.new(data)
	on_check_push_availability_success.emit(obj)

func _on_check_push_availability_failure(data: String):
	var obj = RuStoreError.new(data)
	on_check_push_availability_failure.emit(obj)


# Get token
# @brief Получить текущий push-токен пользователя.
func get_token():
	_clientWrapper.getToken()

func _on_get_token_success(data: String):
	on_get_token_success.emit(data)

func _on_get_token_failure(data: String):
	var obj = RuStoreError.new(data)
	on_get_token_failure.emit(obj)


# Delete token
# @brief Удалить текущий push-токен пользователя.
func delete_token():
	_clientWrapper.deleteToken()
	
func _on_delete_token_success():
	on_delete_token_success.emit()

func _on_delete_token_failure(data: String):
	var obj = RuStoreError.new(data)
	on_delete_token_failure.emit(obj)


# Subscribe to topic
# @brief Подписка на push-уведомления по топику.
# @param topicName Название топика.
func subscribe_to_topic(topicName: String):
	_clientWrapper.subscribeToTopic(topicName)

func _on_subscribe_to_topic_success():
	on_subscribe_to_topic_success.emit()

func _on_subscribe_to_topic_failure(data: String):
	var obj = RuStoreError.new(data)
	on_subscribe_to_topic_failure.emit(obj)


# Unsubscribe from topic
# @brief Отписка от топика.
# @param topicName Название топика.
func unsubscribe_from_topic(topicName: String):
	_clientWrapper.unsubscribeFromTopic(topicName)

func _on_unsubscribe_from_topic_success():
	on_unsubscribe_from_topic_success.emit()

func _on_unsubscribe_from_topic_failure(data: String):
	var obj = RuStoreError.new(data)
	on_unsubscribe_from_topic_failure.emit(obj)


# Service events
func _on_new_token(token: String):
	on_new_token.emit(token)

func _on_message_received(data: String):
	var obj = RuStoreRemoteMessage.new(data)
	on_message_received.emit(obj)

func _on_deleted_messages():
	on_deleted_messages.emit()

func _on_error(data: Dictionary):
	var array = Array()
	for key in data.keys():
		var value = data.get(key)
		array.append(RuStoreError.new(value))
	on_error.emit(array)

func _get_nullable_cause(json: String):
	var cause = null
	if (json != ""):
		cause = RuStoreError.new(json)

# Log events
func _on_log_verbose(message: String, cause: String):
	on_log_verbose.emit(message, _get_nullable_cause(cause))

func _on_log_debug(message: String, cause: String):
	on_log_debug.emit(message, _get_nullable_cause(cause))

func _on_log_info(message: String, cause: String):
	on_log_info.emit(message, _get_nullable_cause(cause))

func _on_log_warn(message: String, cause: String):
	on_log_warn.emit(message, _get_nullable_cause(cause))

func _on_log_error(message: String, cause: String):
	on_log_error.emit(message, _get_nullable_cause(cause))

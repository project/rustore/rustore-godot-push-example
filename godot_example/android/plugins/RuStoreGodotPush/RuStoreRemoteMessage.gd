class_name RuStoreRemoteMessage extends Object

var messageId = null
var priority: int = 0
var ttl: int = 0
var from: String = ""
var collapseKey = null
var data: Dictionary = {}
var rawData = null
var notificationData: RuStoreNotification = null

func _init(json: String = ""):
	if json != "":
		var obj = JSON.parse_string(json)
		
		if obj.has("messageId"):
			messageId = obj["messageId"]
		
		priority = int(obj["priority"])
		ttl = int(obj["ttl"])
		from = obj["from"]
		
		if obj.has("collapseKey"):
			collapseKey = obj["collapseKey"]
		
		if obj.has("data"):
			data = obj["data"]
		
		if obj.has("rawData"):
			rawData = obj["rawData"]
		
		if obj.has("notification"):
			var notificationJson: String = JSON.stringify(obj["notification"])
			notificationData = RuStoreNotification.new(notificationJson)

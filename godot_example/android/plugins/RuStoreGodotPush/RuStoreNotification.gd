class_name RuStoreNotification extends Object

var title = null
var body = null
var channelId = null
var imageUrl = null
var color = null
var icon = null
var clickAction = null
var clickActionType = null

func _init(json: String = ""):
	if json != "":
		var obj = JSON.parse_string(json)
		
		if obj.has("title"):
			title = obj["title"]
		
		if obj.has("body"):
			body = obj["body"]
		
		if obj.has("channelId"):
			channelId = obj["channelId"]
		
		if obj.has("imageUrl"):
			imageUrl = obj["imageUrl"]
		
		if obj.has("color"):
			color = obj["color"]
		
		if obj.has("icon"):
			icon = obj["icon"]
		
		if obj.has("clickAction"):
			clickAction = obj["clickAction"]
		
		if obj.has("clickActionType"):
			clickActionType = ERuStoreClickActionType.Item.get(obj["clickActionType"])

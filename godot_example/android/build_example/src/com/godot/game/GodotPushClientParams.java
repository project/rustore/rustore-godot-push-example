package com.godot.game;

import android.content.Context;
import com.vk.push.common.clientid.ClientId;
import com.vk.push.common.clientid.ClientIdCallback;
import com.vk.push.common.clientid.ClientIdType;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ru.rustore.godot.pushclient.RuStoreGodotLogger;
import ru.rustore.godot.pushclient.RuStoreGodotPushClientParams;
import ru.rustore.sdk.pushclient.common.logger.Logger;

public class GodotPushClientParams extends RuStoreGodotPushClientParams {
	
	public final String CLIENT_ID_VALUE = "your_gaid_or_oaid";
	public final ClientIdType CLIENT_ID_TYPE = ClientIdType.GAID;

	public GodotPushClientParams(Context context) {
        super(context);
    }

    @NonNull
    @Override
    public Logger getLogger() {
        return RuStoreGodotLogger.INSTANCE;
    }

    @Override
    public boolean getTestModeEnabled() {
        return false;
    }

    @Nullable
    @Override
    public ClientIdCallback getClientIdCallback() {
        return () -> new ClientId(CLIENT_ID_VALUE, CLIENT_ID_TYPE);
    }
}

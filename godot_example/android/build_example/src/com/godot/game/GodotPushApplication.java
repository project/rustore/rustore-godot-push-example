package com.godot.game;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.Map;
import ru.rustore.sdk.pushclient.RuStorePushClient;
import ru.rustore.godot.pushclient.RuStoreGodotLogger;

public class GodotPushApplication extends Application {
	public final String PROJECT_ID = "-Yv4b5cM2yfXm0bZyY6Rk7AHX8SrHmLI";
	//public final String PROJECT_ID = "6RUviJhYqgNXDXAIL5wqkAP1Rdnd4JmY";

	public final String METRIC_TYPE = "godot";

	@Override
	public void onCreate() {
		super.onCreate();

		Map<String, String> internalConfig = Map.of("type", METRIC_TYPE);
		
		RuStorePushClient.INSTANCE.init(
            this,
            PROJECT_ID,
            RuStoreGodotLogger.INSTANCE,
			internalConfig,
			null,
			false,
			null,
			null
        );
	}
}

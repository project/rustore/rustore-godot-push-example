package com.godot.game;
 
import org.godotengine.godot.FullScreenGodotApp;
import android.os.Bundle;
 
public class GodotPushApp extends FullScreenGodotApp {
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.GodotAppMainTheme);
        super.onCreate(savedInstanceState);
    }
}

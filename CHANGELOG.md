## История изменений

### Release 6.8.0
- Версия SDK push 6.8.0.

### Release 6.7.0
- Версия SDK push 6.7.0.

### Release 6.6.1
- Версия SDK push 6.6.1.

### Release 6.5.1
- Версия SDK push 6.5.1.

### Release 6.4.0
- Версия SDK push 6.4.0.

### Release 6.3.0
- Версия SDK push 6.3.0.

### Release 6.2.1
- Версия SDK push 6.2.1.
- Все поля класса `RuStoreNotification` и поля `messageId`, `collapseKey`, `rawData`, `notificationData` класса `RuStoreRemoteMessage` теперь имеют по умолчанию значение `null`.

### Release 6.1.0
- Версия SDK push 6.1.0.

### Release 6.0.0
- Версия SDK push 6.+.
- В классс Notification добавлено поле clickActionType.

### Release 2.0.0
- Версия SDK push 2.+.
- Пуш теперь описывается классами RuStoreRemoteMessage.gd и RuStoreNotification.gd вместо Dictionary.
- В класс RuStoreError добавлено поле name, которое содержит simpleName класса ошибки.
- Добавлены методы set_error_handling и get_error_handling для автоматической обработки ошибок.
- Исправлена ошибка сериализации Uri.

### Release 1.4
- Версия SDK push 1.+.
- Инициализация через meta-data.

### Release 1.2
- Версия SDK push 1.2.0.
- Добавлен функционал [сегментов](https://www.rustore.ru/help/sdk/push-notifications/using-segments)

### Release 1.0
- Версия SDK push 1.0.0.
